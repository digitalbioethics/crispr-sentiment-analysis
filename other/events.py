"""
This file is supposed to be manually created.

Code in peak_detection.py will modify this file and generate the file events_meta.json file with additional data to these events
"""

from datetime import datetime

EVENTS = [
  {
    'time': datetime(2015, 12, 3),
    'event_time': datetime(2015, 12, 1),
    'description': 'International summit on human gene editing (National Academy of Sciences, Washington D.C.)',
    'urls': [
      'http://nationalacademies.org/gene-editing/Gene-Edit-Summit/index.htm',
      'https://www.technologyreview.com/s/543941/everything-you-need-to-know-about-crispr-gene-editings-monster-year/'
    ],
    'key': 'human_gene_editing_summit',
    'title': 'First summit on human gene editing in Washington D.C.',
    'category': 'news',
    'sentiment': 'neutral',
    'marker': '',
    'visible': True
  },
  {
    'time': datetime(2016, 1, 20),
    'event_time': datetime(2016, 1, 20),
    'description': 'Founder of Broad Institute Eric Lander downplays Berkley\'s influence on the discovery of CRISPR (Cell article "The Heroes of CRISPR").',
    'urls': [
      'https://www.cell.com/cell/fulltext/S0092-8674(15)01705-5',
      'https://www.wired.com/2016/01/crispr-twitter-fight/?mbid=social_twitter'
    ],
    'key': 'cell_heroes_of_crispr',
    'title': 'Cell paper: Heroes of CRISPR',
    'category': 'news',
    'sentiment': 'neutral',
    'marker': '',
    'visible': False
  },
  {
    'time': datetime(2016, 6, 24),
    'event_time': datetime(2016, 6, 22),
    'description': 'First proposed human test of CRISPR passes initial safety review',
    'urls': [
      'https://www.sciencemag.org/news/2016/06/first-proposed-human-test-crispr-passes-initial-safety-review?utm_source=newsfromscience&utm_medium=twitter&utm_campaign=humancrispr-5196',
      'https://institutions.newscientist.com/article/2094832-first-human-crispr-trial-given-go-ahead-your-questions-answered/'
    ],
    'key': 'first_human_trial_review',
    'title': 'U.S. proposal for human trials passes safety reviews',
    'category': 'news',
    'sentiment': 'positive',
    'marker': '',
    'visible': True
  },
  {
    'time': datetime(2016, 11, 18),
    'event_time': datetime(2016, 11, 15),
    'description': 'Chinese research group used CRISPR on a human patient the first time. (10.1038/nature.2016.20988)',
    'urls': [
      'https://www.nature.com/news/1.20988?WT.mc_id=TWT_NatureNews'
    ],
    'key': 'first_use_in_humans_china',
    'title': 'First time use of CRISPR on humans in China',
    'category': 'study',
    'sentiment': 'positive',
    'marker': 'a',
    'visible': True
  },
  {
    'time': datetime(2017, 2, 17),
    'event_time': datetime(2017, 2, 15),
    'description': 'U.S. Patent Office decides in favor of the Broad institute in the patent conflict.',
    'urls': [
      'https://www.washingtonpost.com/news/speaking-of-science/wp/2017/02/15/broad-institute-scientist-prevails-in-epic-patent-fight-over-crispr/?noredirect=on&utm_term=.8f986e1e19e9'
    ],
    'key': 'patent_conflict',
    'title': 'Broad Institute prevails in patent conflict',
    'sentiment': 'neutral',
    'category': 'patent',
    'marker': 'b',
    'visible': True
  },
  {
    'time': datetime(2017, 6, 2),
    'event_time': datetime(2017, 5, 30),
    'description': 'Nature paper is published which reveals unexpected side-effects in experiments with mice (Schaefer, Kellie A., et al. "Unexpected mutations after CRISPR–Cas9 editing in vivo." Nature methods 14.6 (2017): 547.)',
    'urls': [
      'https://www.nature.com/articles/nmeth.4293',
      'https://www.rt.com/viral/390211-crispr-gene-editing-mutations/'
    ],
    'key': 'side_effects_mice_nature_paper',
    'title': 'Experiments with CRISPR on mice reveal unexpected side-effects',
    'sentiment': 'negative',
    'category': 'study',
    'marker': 'c',
    'visible': True
  },
  {
    'time': datetime(2017, 7, 5),
    'event_time': datetime(2017, 7, 5),
    'description': 'Rebuttal of mice study from May. Call for withdrawal of paper.',
    'urls': [
      'https://institutions.newscientist.com/article/2139939-crispr-gene-editing-technique-is-probably-safe-study-confirms/?utm_campaign=Echobox&utm_medium=Social&utm_source=Twitter'
    ],
    'key': 'rebuttal_nature_paper',
    'title': 'Rebuttal of Nature paper',
    'sentiment': 'neutral',
    'category': 'study',
    'marker': '',
    'visible': False
  },
  {
    'time': datetime(2017, 7, 15),
    'event_time': datetime(2017, 7, 12),
    'description': 'GIF of running horse is encoded into DNA using CRISPR',
    'urls': [
      'https://www.theatlantic.com/science/archive/2017/07/scientists-can-use-crispr-to-store-images-and-movies-in-bacteria/533400/?utm_source=atltw'
    ],
    'key': 'gif_encoded',
    'title': 'A GIF of a running horse was encoded into DNA',
    'sentiment': 'positive',
    'category': 'study',
    'marker': '',
    'visible': True
  },
  {
    'time': datetime(2017, 7, 27),
    'event_time': datetime(2017, 7, 27),
    'description': 'CRISPR was used for the first time in humans in the US',
    'urls': [
      'https://www.wired.com/story/scientists-crispr-the-first-human-embryos-in-the-us-maybe/?mbid=social_twitter'
    ],
    'key': 'first_use_in_humans_us',
    'title': 'First trial of CRISPR in humans in the US',
    'sentiment': 'positive',
    'category': 'news',
    'marker': '',
    'visible': False
  },
  {
    'time': datetime(2017, 8, 4),
    'event_time': datetime(2017, 8, 2),
    'description': 'CRISPR fixes disease gene in viable human embryos',
    'urls': [
      'https://www.nature.com/news/1.22382?WT.mc_id=TWT_NatureNews&sf103457530=1'
    ],
    'key': 'fix_disease_gene_human_embryo',
    'title': 'CRISPR successfully fixes a gene in viable human embryos',
    'sentiment': 'positive',
    'category': 'study',
    'marker': '',
    'visible': True
  },
  {
    'time': datetime(2017, 8, 31),
    'event_time': datetime(2017, 8, 31),
    'description': 'Skepticism surfaces over CRISPR human embryo editing claims',
    'urls': [
      'https://www.sciencemag.org/news/2017/08/skepticism-surfaces-over-crispr-human-embryo-editing-claims?utm_source=newsfromscience&utm_medium=twitter&utm_campaign=crisprskeptic-14999'
    ],
    'key': 'skepticism_towards_fix_disease_gene_human_embryo',
    'title': 'Skepticisim over recent work',
    'sentiment': 'negative',
    'category': 'news',
    'marker': '',
    'visible': False
  },
  {
    'time': datetime(2017, 10, 26),
    'event_time': datetime(2017, 10, 25),
    'description': 'Nature study which allows base editing of single nucleotides',
    'urls': [
      'https://www.nature.com/articles/nature24644',
      'https://www.sciencenews.org/article/new-crispr-gene-editors-can-fix-rna-and-dna-one-typo-time',
      'https://www.newscientist.com/article/2151455-weve-evolved-an-even-more-powerful-form-of-crispr-gene-editing/?cmpid=SOC|NSNS|2017-Echobox&utm_campaign=Echobox&utm_medium=Social&utm_source=Twitter#link_time=1509010146'
    ],
    'key': 'nature_base_editing',
    'title': 'Study on advances in single nucleotide editing with CRISPR',
    'sentiment': 'positive',
    'category': 'study',
    'marker': '',
    'visible': True
  },
  {
    'time': datetime(2018, 1, 8),
    'event_time': datetime(2018, 1, 5),
    'description': 'Study reveals that most people could be immune to CRISPR',
    'urls': [
      'https://www.technologyreview.com/f/609904/uh-oh-crispr-might-not-work-in-people/',
      'https://www.biorxiv.org/content/10.1101/243345v1'
    ],
    'key': 'crispr_immunity',
    'title': 'Study reveals that certain people might be immune to CRISPR',
    'sentiment': 'negative',
    'category': 'study',
    'marker': '',
    'visible': False
  },
  {
    'time': datetime(2018, 1, 21),
    'event_time': datetime(2018, 1, 19),
    'description': 'McGill study on improved techniques for fixing genes.',
    'urls': [
      'https://www.newscientist.com/article/2158970-new-crispr-method-could-take-gene-editing-to-the-next-level/?cmpid=SOC|NSNS|2017-Echobox&utm_campaign=Echobox&utm_medium=Social&utm_source=Twitter#link_time=1516473814',
    ],
    'key': 'mc_gill_study',
    'title': 'Study on advances in CRISPR technology',
    'sentiment': 'positive',
    'category': 'news',
    'marker': '',
    'visible': False
  },
  {
    'time': datetime(2018, 3, 29),
    'event_time': datetime(2018, 3, 29),
    'description': 'Paper suggesting mutations in mice in Nature got retracted.',
    'urls': [
      'https://www.technologyreview.com/s/610726/crispr-may-not-cause-hundreds-of-rogue-mutations-after-all/?utm_campaign=Technology+Review&utm_medium=social&utm_source=twitter.com'
    ],
    'key': 'side_effects_nature_paper_retracted',
    'title': 'Rectraction of Nature paper',
    'sentiment': 'neutral',
    'category': 'news',
    'marker': '',
    'visible': False
  },
  {
    'time': datetime(2018, 6, 14),
    'event_time': datetime(2018, 6, 11),
    'description': 'Two studies warn about links to cancer',
    'urls': [
      'https://www.scientificamerican.com/article/crispr-edited-cells-linked-to-cancer-risk-in-2-studies/?utm_source=twitter&utm_medium=social&utm_campaign=sa-editorial-social&utm_content=&utm_term=health_&sf191624790=1'
    ],
    'key': 'crispr_cancer',
    'title': 'New studies show possible links of CRISPR treatment to cancer',
    'sentiment': 'negative',
    'category': 'study',
    'marker': '',
    'visible': False
  },
  {
    'time': datetime(2018, 7, 19),
    'event_time': datetime(2018, 7, 16),
    'description': 'Study by Wellcome Sanger institute warns CRISPR could cause big deletions/rearrangements of DNA.',
    'urls': [
      'https://institutions.newscientist.com/article/2174149-crispr-gene-editing-is-not-quite-as-precise-and-as-safe-as-thought/?utm_campaign=Echobox&utm_medium=SOC&utm_source=Twitter',
      'https://www.statnews.com/2018/07/16/crispr-potential-dna-damage-underestimated/?utm_content=bufferb7166&utm_medium=social&utm_source=twitter&utm_campaign=twitter_organic'
    ],
    'key': 'crispr_side_effects_1',
    'title': 'Study shows the potential for side effects (e.g. deletions) of CRISPR',
    'sentiment': 'negative',
    'category': 'study',
    'marker': 'd',
    'visible': True
  },
  {
    'time': datetime(2018, 11, 29),
    'event_time': datetime(2018, 11, 26),
    'description': 'The MIT Technology Review reveals story about Chinese scientist ("CRISPR babies")',
    'urls': [
      'https://www.apnews.com/4997bb7aa36c45449b488e19ac83e86d'
    ],
    'key': 'crispr_babies',
    'title': '"CRISPR babies" scandal',
    'sentiment': 'negative',
    'category': 'news',
    'marker': 'e',
    'visible': True
  },
  {
    'time': datetime(2019, 2, 4),
    'event_time': datetime(2019, 2, 3),
    'description': 'Biohackers encode malware in strand of DNA',
    'urls': [
      'https://www.wired.com/story/malware-dna-hack/'
    ],
    'key': 'biohack_malware',
    'title': 'Biohackers encode a malware program into DNA',
    'sentiment': 'neutral',
    'category': 'news',
    'marker': 'f',
    'visible': True
  },
  {
    'time': datetime(2019, 2, 23),
    'event_time': datetime(2019, 2, 21),
    'description': 'He’s attempt to delete the CCR5 gene could inadvertently have changed the girls’ brains in ways that affect cognition and memory, new research suggests.',
    'key': 'crispr_side_effects_2',
    'title': 'Potential side effects of "CRISPR babies" experiment unveiled',
    'sentiment': 'negative',
    'category': 'news',
    'marker': '',
    'visible': False
  },
  {
    'time': datetime(2019, 4, 19),
    'event_time': datetime(2019, 4, 17),
    'description': 'New study to use Crispr/Cas3 to shred longer stretches of DNA shows promising results',
    'urls': [
      'https://www.engadget.com/2019/04/17/crispr-cas3-dna-shredder/',
      'https://thenextweb.com/science/2019/04/17/new-crispr-tool-could-eradicate-viral-diseases-with-long-range-dna-shredding/'
    ],
    'key': 'crispr_study_success',
    'title': 'Study shows promising results of CRISPR in combatting viral diseases',
    'sentiment': 'positive',
    'category': 'study',
    'marker': '',
    'visible': True
  }
]
