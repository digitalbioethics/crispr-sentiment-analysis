# Combining Crowdsourcing and Deep Learning to Assess Public Opinion on CRISPR-Cas9
This repository contains all code and data (which could be made public) for the project on sentiment towards CRISPR/Cas9. The paper reporting the results of the analysis is published in JMIR: https://doi.org/10.2196/17830

## Installation
```
conda env create -f environment.yml
```
Python version 3.6

The environment (Python version 3.6) contains the following packages:
```
- scikit-learn
- numpy
- munch
- matplotlib
- python
- shapely
- hyperopt
- visdom
- dill
- spacy
- nltk
- pybind11
- pandas
- pytorch
- statsmodels
- geopandas
- seaborn
```
Optional packages for GPU support: `torchvision`, `cudatoolkit`

Installation takes less than two minutes (MacBook Pro, 13-inch, 2017; OS Version 10.14.6; 2.3 GHz Intel Core i5 Processor; 16 GB Memory).

## Usage
Clone this repository `cd` into it and initialize the submodules by running:
```
git submodule update --init
```

This initializes two submodules:
* `preprocess`: All code which is used for the processing of raw data
* `text-classification`: All code which is used for training text classification models

Then download all preprocess data (56.3MB) from the data repository (see below) and copy it into the preprocess module:
```
wget https://gitlab.ethz.ch/digitalbioethics/crispr-sentiment-analysis-data/-/raw/master/preprocess_data.zip
unzip preprocess_data.zip
cp -r preprocess_data/* preprocess/data/
rm preprocess_data.zip
rm -r preprocess_data
```

To use the text-classification submodule (optional) also download training specification and pre-trained models (3.48GB):
```
wget https://gitlab.ethz.ch/digitalbioethics/crispr-sentiment-analysis-data/-/raw/master/text_classification_data.zip
unzip text_classification_data.zip
cp -r text_classification_data/* text-classification/data/
rm text_classification_data.zip
rm -r text_classification_data
```

### Data
As we are not able to share raw data. We can only provide the data for certain steps of the analysis pipeline. The data to reproduce the results can be found here: https://gitlab.ethz.ch/digitalbioethics/crispr-sentiment-analysis-data
* `2_cleaned`: Processsed raw data (corresponds to dataset `D2` in Figure 5 of paper)
* `3_sampled`: Sampled data (corresponds to dataset `S0`)
* `4_labelled/mturk`: Annotation data by Mturk workers (corresponds to `A0`)
* `5_labels_cleaned`: Contains cleaned (merged) annotation set (used for training/testing) (corresponds to `A1` and `A2`)
* `6_predicted`: Prediction of models (corresponds to `P0`, `P1` and `P2`)
* `other`: Hashtags and themes data

Note that certain columns had to be removed or have been anonymized.

### Code
All code for the analysis can be found in `analysis`.

Run the following scripts (runtime < 1 min) to reproduce the results of the paper (takes < 2 min):
```
cd analysis
python basic_stats.py
python organism_heatmap.py
python rankflow_hashtags.py
python sent_vs_time.py
python significance_testing.py
python themes.py
```


## Authors
Feel free to contact any of the authors if you have questions

* Martin Müller (martin.muller@epfl.ch)
* Manuel Schneider (manuel.schneider@hest.ethz.ch)
