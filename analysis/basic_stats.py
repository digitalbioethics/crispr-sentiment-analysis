import sys; sys.path.append('..');
from utils.helpers import save_table, save_fig, paper_plot, label_subplots, get_all_data
import pandas as pd

print("loading data...")
df = get_all_data(include_all_data=False)

print("\ndataset sizes")
print("|d0| = {}, |d1| = {}, |d2| = {}, |p0| = {}, |p1| = {}".format(
    len(df[df.d0]), len(df[df.d1]), len(df[df.d2]), len(df[df.p0]), len(df[df.p1])
))

dfy = df[df.d0]['id'].groupby(df[df.d0].index.year).count()
print("\noverall tweets per year")
print(dfy)
print("total = {}".format(dfy.sum()))

dfo = pd.DataFrame({
    'counts': df[df.p1].label_organism.value_counts(),
    'fraction': df[df.p1].label_organism.value_counts(normalize=True).round(3)
})
print("\norganism class counts and fractions")
print(dfo)
print("total = {}".format(dfo['counts'].sum()))


dfs = pd.DataFrame({
    'counts': df[df.p1].label_sentiment.value_counts(),
    'fraction': df[df.p1].label_sentiment.value_counts(normalize=True).round(3)
})
print("\nsentiment class counts and fractions")
print(dfs)
print("total = {}".format(dfs['counts'].sum()))

print("")
