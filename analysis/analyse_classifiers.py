import os
import json
import sys; sys.path.append('..');
from utils.helpers import save_fig, notebook_plot, paper_plot, find_project_root, label_subplots
import matplotlib.pyplot as plt
import matplotlib.ticker as tkr
import pandas as pd
import glob
import seaborn as sns

class AnalyseClassifiers:
    def __init__(self):
        pass

    def load_data(self, pattern=''):
        df = self.collect_results()
        if len(df) > 0:
            df.set_index('name', inplace=True)
            df = df[df.index.str.contains(r'{}'.format(pattern))]
            df.dropna(axis='columns', how='all', inplace=True)
        if len(df) == 0:
            raise Exception('No data available under given pattern')
        return df

    def collect_results(self, run='*'):
        run_path = os.path.join(find_project_root(), 'text-classification', 'output', run)
        folders = glob.glob(run_path)
        results = []
        for f in folders:
            if os.path.isdir(f):
                config_path = os.path.join(f, 'run_config.json')
                test_output_path = os.path.join(f, 'test_output.json')
                try:
                    with open(config_path, 'r') as f_p:
                        run_config = json.load(f_p)
                    with open(test_output_path, 'r') as f_p:
                        test_output = json.load(f_p)
                except FileNotFoundError:
                    continue
                results.append({**run_config, **test_output})
        return pd.DataFrame(results)

    def get_random_models(self, models=['random', 'weighted_random', 'dummy'], pattern='*'):
        df = self.load_data()
        if len(df) == 0:
            raise Exception('No models found')
        df = df[df.index.str.contains(r'{}'.format('|'.join(models)))]
        df = df[df.index.str.contains(r'{}'.format(pattern))]
        if len(df) == 0:
            raise Exception('No random models found')
        return df

    @notebook_plot
    def plot_metrics(self, df):
        df = self.load_data(pattern='*')
        metrics = ['accuracy', 'precision_macro', 'recall_macro', 'f1_macro']
        df_metrics = df[metrics].dropna()
        df_metrics.sort_values('accuracy', inplace=True, ascending=False)
        df_metrics = df_metrics.iloc[:-1]
        _min, _max = max(0, df_metrics.min().min() - 0.1), min(df_metrics.max().max() + 0.1, 1)
        fig, ax = plt.subplots()
        ax = df_metrics.plot.barh(y=metrics, xlim=[_min, _max], ax=ax)
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        ax.set_ylabel('')
        save_fig(fig, 'analyse_classifiers_default', version=4)

    @notebook_plot
    def plot_metrics_with_random(self):
        model = ['sentiment', '(?<!reason_)sentiment', 'organism', 'related', 'other_topic', 'context'][1]
        version = 14
        df_random = self.get_random_models(pattern=model)
        df = self.load_data(pattern='bert_best_{model}|fasttext_{model}|bow_{model}'.format(model = model))
        df = pd.concat([df_random, df], sort=True)
        plot_metrics = ['precision', 'recall', 'accuracy', 'f1']
        df = self._format_metrics(df, plot_metrics)
        df = df[plot_metrics]
        _min, _max = max(0, df.min().min() - 0.1), min(df.max().max() + 0.1, 1)
        fig, ax = plt.subplots()
        ax = df.plot.barh(y=plot_metrics, xlim=[_min, _max], ax=ax)
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        ax.set_ylabel('')
        save_fig(fig, 'analyse_classifiers_default', version=version)

    @paper_plot
    def plot_metrics_final(self):
        version = 15
        best_models = {
                'sentiment': {
                    'random': 'random_sentiment',
                    'majority': 'dummy_sentiment',
                    'bag of words': 'bow_sentiment',
                    'fastText': 'fasttext_sentiment_lc_v1_run_39',
                    'BERT': 'bert_sentiment_lc_run_6',
                    'BERT (ft)': 'bert_large_ft_sentiment_filters_3'
                    },
                'relevance': {
                    'random': 'random_related',
                    'majority': 'dummy_related',
                    'bag of words': 'bow_related',
                    'fastText': 'fasttext_related',
                    'BERT': 'bert_best_related_bert',
                    'BERT (ft)': 'bert_large_ft_related'

                    },
                'organism': {
                    'random': 'random_organism',
                    'majority': 'dummy_organism',
                    'bag of words': 'bow_organism',
                    'fastText': 'fasttext_organism',
                    'BERT': 'bert_best_organism_bert',
                    'BERT (ft)': 'bert_large_ft_organism'
                    }
                }
        best_model_names = [n for p, _ in best_models.items() for _, n in best_models[p].items()]
        df = self.collect_results()
        df = df[df.name.isin(best_model_names)]
        df.set_index('name', inplace=True)
        plot_metrics = ['precision', 'recall', 'accuracy', 'f1']
        df = self._format_metrics(df, plot_metrics)
        df = df[plot_metrics]

        # plot
        # figsize = (3.5039, 2.1656)
        figsize = (7.2047, 4.4527) # double column
        fig, all_axes = plt.subplots(3, 1, figsize=figsize, sharex=True)
        for ax, (predictor, models) in zip(all_axes, best_models.items()):
            _df = df.loc[models.values()]
            _df['model_name'] = models.keys()
            _df.set_index('model_name', inplace=True)
            ax = _df.plot.barh(y=plot_metrics, xlim=[0, 1], ax=ax, legend=False)
            ax.set_ylabel(predictor)
            ax.grid(True)

        ax.set_xlabel('Test score')
        all_axes[0].legend(loc='upper right')
        label_subplots(all_axes, offset_points=(-60, 0))
        plt.subplots_adjust(wspace=0, hspace=0.1)
        save_fig(fig, 'analyse_classifiers_default', version=version)

    @notebook_plot
    def plot_learning_curve(self):
        model = ['sentiment', '(?<!reason_)sentiment', 'organism', 'related', 'other_topic', 'context'][1]
        version = 8
        # pattern = 'fasttext_sentiment_lc_v1_run_*'
        pattern = 'bert_sentiment_lc_v5_run_*'
        df = self.load_data(pattern=pattern)
        plot_metrics = ['precision', 'recall', 'accuracy', 'f1', 'precision_positive', 'precision_negative', 'precision_neutral']
        # plot_metrics = ['precision', 'recall', 'accuracy', 'f1']
        df = self._format_metrics(df, plot_metrics)
        fig, ax = plt.subplots()
        df.sort_values('learning_curve_fraction', inplace=True)
        df_means = df.groupby('learning_curve_repetition_index').mean()
        df_std = df.groupby('learning_curve_repetition_index').std()
        for metric in plot_metrics:
            mean_plt = df_means.plot(x='learning_curve_fraction', y=metric, ax=ax)
            df.plot.scatter(x='learning_curve_fraction', y=metric, ax=ax, c=mean_plt.lines[-1].get_color(), s=3)
        ax.set_ylabel('Metric')
        ax.set_title(pattern)
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        ax.grid()
        save_fig(fig, 'learning_curve', version=version)

    @notebook_plot
    def plot_compare_learning_curves(self):
        version = 2
        # pattern = 'fasttext_sentiment_lc_v1_run_*'
        dfs = []
        patterns = ['bert_sentiment_lc_v5_run_*', 'bert_sentiment_lc_v4_run_*']
        # plot_metrics = ['precision', 'recall', 'accuracy', 'f1', 'precision_positive', 'precision_negative', 'precision_neutral']
        plot_metrics = ['f1']
        for p in patterns:
            df = self.load_data(pattern=p)
            df = self._format_metrics(df, plot_metrics)
            dfs.append(df)
        # plot_metrics = ['precision', 'recall', 'accuracy', 'f1']
        fig, ax = plt.subplots()
        for i, df in enumerate(dfs):
            df.sort_values('learning_curve_fraction', inplace=True)
            df_means = df.groupby('learning_curve_repetition_index').mean()
            df_std = df.groupby('learning_curve_repetition_index').std()
            for metric in plot_metrics:
                mean_plt = df_means.plot(x='learning_curve_fraction', y=metric, ax=ax, label=patterns[i])
                df.plot.scatter(x='learning_curve_fraction', y=metric, ax=ax, c=mean_plt.lines[-1].get_color(), s=3)
            ax.set_ylabel('Metric')
            ax.set_title(' vs. '.join(patterns))
            ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
            ax.grid()
        save_fig(fig, 'compare_learning_curves', version=version)

    @notebook_plot
    def visualize_grid_search(self):
        def _fmt(s, _fmt):
            try:
                if _fmt == 'sci':
                   return s.apply(lambda s: '{:.0e}'.format(s))
                else:
                    return s.astype(_fmt)
            except:
                return s
        # load data
        pattern = 'bert_ft_gridsearch_other_topic'
        version = 11
        df = self.load_data(pattern=pattern)
        plot_metrics = ['precision', 'recall', 'accuracy', 'f1']
        df = self._format_metrics(df, plot_metrics)
        if 'bert_ft_gridsearch_v?' in pattern:
            _vars = ['num_epochs', 'learning_rate', 'train_batch_size']
            _parse_fmt = [int, 'sci', int]
        if 'bert_ft_gridsearch' in pattern:
            _vars = ['learning_rate', 'num_epochs']
            _parse_fmt = ['sci', int]
        elif 'bert' in pattern:
            _vars = ['num_epochs', 'learning_rate']
            _parse_fmt = [int, 'sci']
        elif 'fasttext' in pattern:
            _vars = ['dim', 'n_grams', 'learning_rate']
            _parse_fmt = [int, int, 'sci']
        else:
            _vars = ['num_epochs', 'learning_rate']
            _parse_fmt = [int, 'sci']
        _heat = 'f1'
        grid_size = len(_vars)
        fig, axes = plt.subplots(grid_size - 1, grid_size - 1, sharex='col', sharey='row')
        if grid_size == 2:
            axes = [[axes]]
        fig.suptitle(pattern)
        fig.subplots_adjust(wspace=0.1, hspace=0.15)
        for i in range(1, grid_size):
            for j in range(grid_size - 1):
                ax = axes[i-1][j]
                if j < i:
                    # create pivot
                    _df = df[~df[[_vars[i], _vars[j]]].duplicated()].copy()
                    pivot = _df.pivot(_vars[i], _vars[j], _heat)
                    # format properly
                    pivot.index = _fmt(pd.Series(pivot.index), _parse_fmt[i])
                    pivot.columns = _fmt(pd.Series(pivot.columns), _parse_fmt[j])
                    sns.heatmap(pivot, annot=True, linewidths=.5, ax=ax, fmt='.3f', annot_kws={'fontsize': 4}, cbar=False, cmap='YlOrRd', yticklabels=True)
                    ax.get_yaxis().set_visible(False)
                    ax.get_xaxis().set_visible(False)
                    if j == 0:
                        ax.get_yaxis().set_visible(True)
                        ax.yaxis.set_tick_params(rotation=0)
                        ax.get_yaxis().set_label_coords(-.3, 0.5)
                    if i == len(_vars) - 1:
                        ax.get_xaxis().set_visible(True)
                        ax.get_xaxis().set_label_coords(.5, -.3)
                        ax.xaxis.set_tick_params(rotation=0)
                    # make fontsize smaller
                    for item in ([ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
                        item.set_fontsize(4)
                else:
                    ax.axis('off')
        save_fig(fig, 'grid_search', version=version)

    def list_best_models(self):
        def _fmt(s, _fmt):
            if _fmt == 'sci':
               return s.apply(lambda s: '{:.0e}'.format(s))
            else:
                return s.astype(_fmt)
        # params
        pattern = ''
        train_data_contains = '(?<!reason_)sentiment'
        _optimize_for = ['f1', 'precision', 'recall', 'accuracy']

        df = self.load_data(pattern=pattern)
        if train_data_contains != '':
            df = df[df.train_data.str.contains(train_data_contains)]
            if len(df) == 0:
                raise Exception('No train data with expression {} found.'.format(train_data_contains))
        plot_metrics = ['precision', 'recall', 'accuracy', 'f1']
        df = self._format_metrics(df, plot_metrics)
        _vars = ['dim', 'n_grams', 'learning_rate', 'num_epochs', 'train_batch_size', 'f1_negative', 'f1_positive', 'f1_neutral']
        _top = 40
        _parse_fmt = {'learning_rate': 'sci', 'dim': int, 'n_grams': int, 'num_epochs': int, 'train_batch_size': int, 'f1_positive': float, 'f1_negative': float, 'f1_neutral': float}
        output_vars = []
        for i, k in _parse_fmt.items():
            if i in df:
                try:
                    df[i] = _fmt(df[i], k)
                except:
                    pass
                output_vars.append(i)
        df = df[output_vars + _optimize_for]
        df = df.sort_values(_optimize_for, ascending=False)[:_top]
        print('Models of pattern: "{}", training data with pattern: "{}", optimize for {}\n\n'.format(pattern, train_data_contains, _optimize_for[0]))
        print(df)

    def _format_metrics(self, df, plot_metrics, sort_by='f1', averaging='macro'):
        """Computes f1 from precision and recall manually as harmonic mean. Renames columns."""
        metrics = ['precision', 'recall']
        metrics_av = [m + '_' + averaging for m in metrics]
        # compute f1 directly from precision and recall (f1 micro/macro averaging option can not be compared to those)
        p = df[metrics_av[0]]
        r = df[metrics_av[1]]
        df['f1'] = 2*p*r/(p+r)
        df.rename(dict(zip(metrics_av, metrics)), inplace=True, axis=1)
        df = df[~df[plot_metrics].isna().any(axis=1)]
        df.sort_values(sort_by, inplace=True, ascending=False)
        return df


if __name__ == "__main__":
    ac = AnalyseClassifiers()
    # ac.plot_metrics_with_random()
    # ac.visualize_grid_search()
    # ac.list_best_models()
    # ac.plot_learning_curve()
    # ac.plot_compare_learning_curves()
    ac.plot_metrics_final()
