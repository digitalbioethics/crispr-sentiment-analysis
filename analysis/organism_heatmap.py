import sys; sys.path.append('..');
import os
from preprocess.utils.helpers import find_folder
from utils.helpers import save_table, save_fig, paper_plot, label_subplots, get_all_data
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
from pandas.plotting import register_matplotlib_converters
from datetime import datetime, timedelta
import matplotlib.dates as mdates


def load_raw_data():
    df = get_all_data(include_all_data=False)
    df = df[df.p1]
    return df

def get_counts(df):
    df_g = df.groupby(['label_organism', 'label_sentiment', df.index.date]).count()[['id']]
    df_g.index.rename('date', level=2, inplace=True)
    df_g.rename(columns={'id': 'counts'}, inplace=True)
    return df_g

def load_data():
    cache_path = os.path.join(find_folder('other'), 'sent_vs_time_counts_organisms.csv')
    if not os.path.isfile(cache_path):
        print('Load data...')
        df = load_raw_data()
        print('Get counts...')
        df = get_counts(df)
        df.to_csv(cache_path)
    print('Reading cached data...')
    df = pd.read_csv(cache_path, index_col=[0,1,2], parse_dates=['date'])
    return df

def get_events(prominence_cutoff=0, s_date=datetime(2000, 1, 1), e_date=datetime(2020, 1, 1)):
    f_path = os.path.join(find_folder('other'), 'events_with_prominence.json')
    df = pd.read_json(f_path, convert_dates=['time', 'event_time'])
    df = df[df.prominence > prominence_cutoff]
    df = df[(df.time > s_date) & (df.time < e_date)]
    return df

@paper_plot
def heatmap():
    df = load_data()
    v = 2

    # group counts by sentiment, organism and year
    df = df.groupby([df.index.get_level_values('label_sentiment'), df.index.get_level_values('label_organism'), df.index.get_level_values('date').year]).sum()

    # compute sentiment
    total = df.groupby(level=[1,2]).sum()
    sentiment = df.loc['positive'].subtract(df.loc['negative'])/total
    sentiment = sentiment.unstack()
    sentiment.columns = sentiment.columns.droplevel()
    sentiment.rename(index={'not_specified': 'unspecified'}, inplace=True)
    sentiment.sort_index(inplace=True)

    # plot sentiment
    fig, ax = plt.subplots(1, 1)
    sns.heatmap(sentiment, annot=True, fmt=".2f", cmap='RdYlGn', vmin=-1, vmax=1, ax=ax)
    ax.set(ylabel='Organism', xlabel='Year')
    ax.tick_params(axis='both', direction='out')

    # save
    save_fig(fig, 'heatmap_organisms', version=v)

@paper_plot
def heatmap_with_counts():
    df = load_data()
    v = 4
    figsize = (7.2047, 2.4) # double column
    # figsize = (3.5039, 2.1656) # single column

    # group counts by sentiment, organism and year
    df = df.groupby([df.index.get_level_values('label_sentiment'), df.index.get_level_values('label_organism'), df.index.get_level_values('date').year]).sum()

    # compute sentiment & counts
    # total = df.groupby(level=[1,2]).sum()
    total = df.loc['positive'].add(df.loc['negative'], fill_value=0).add(df.loc['neutral'], fill_value=0)
    sentiment = df.loc['positive'].subtract(df.loc['negative'])/total
    sentiment = sentiment.unstack()
    sentiment.columns = sentiment.columns.droplevel()
    sentiment.rename(index={'not_specified': 'unspecified'}, inplace=True)
    sentiment.sort_index(inplace=True)
    total = total.unstack()
    total.columns = total.columns.droplevel()
    total.rename(index={'not_specified': 'unspecified'}, inplace=True)
    total.sort_index(inplace=True)

    fig, all_axes = plt.subplots(1, 2, figsize=figsize)
    ax_sentiment, ax_counts = all_axes

    # plot sentiment
    sns.heatmap(sentiment, annot=True, fmt=".2f", cmap='RdYlGn', vmin=-1, vmax=1, ax=ax_sentiment)
    sns.heatmap(total.apply(np.log10), cmap='Blues', annot=True, fmt='.2f', ax=ax_counts)

    ax_sentiment.set_ylabel('Organism')
    ax_counts.yaxis.label.set_visible(False)
    plt.subplots_adjust(wspace=0.25)

    for ax in all_axes:
        ax.set_xlabel('Year')
        ax.tick_params(axis='both', direction='out')

    # save
    save_fig(fig, 'heatmap_organisms', version=v)

@paper_plot
def organism_vs_time():
    df = load_data()
    v = 9
    figsize = (7.2047, 3.5) # double column
    smoothing = 14
    s_date = datetime(2015, 7, 1)
    e_date = datetime(2019, 5, 31)
    plot_organisms = {
            'animals': False,
            'bacteria': False,
            'embryos': True,
            'humans': True,
            'plants': True,
            'not_specified': False
            }
    prominence_cutoff = .2
    lw = .3
    ls = (0, (5, 5))

    # This is needed for to_pydatetime() conversion (conversion between pandas and matplotlib datetime)
    register_matplotlib_converters()

    # group counts by sentiment, organism and year
    df = df.groupby([df.index.get_level_values('label_sentiment'), df.index.get_level_values('label_organism'), df.index.get_level_values('date')]).sum()

    positive = df.loc['positive']
    negative = df.loc['negative']
    neutral = df.loc['neutral']
    total = positive.add(negative, fill_value=0)
    # total = positive.add(negative, fill_value=0).add(neutral, fill_value=0)

    sentiment = positive.subtract(negative, fill_value=0)/total
    sentiment = sentiment.unstack(0)
    sentiment.columns = sentiment.columns.droplevel()
    sentiment = sentiment.rolling(smoothing).median()
    sentiment = sentiment[s_date:e_date]
    sentiment = sentiment.interpolate()

    fig, ax = plt.subplots(1, 1, sharex=True)
    for organism in sentiment.columns:
        if plot_organisms[organism]:
            ax.plot(sentiment.index.to_pydatetime(), sentiment[organism].values, label=organism, zorder=1)

    ax.legend(bbox_to_anchor=(1, 1))

    # add events
    events = get_events(prominence_cutoff=prominence_cutoff, s_date=s_date, e_date=e_date)
    axvlines_artists = []
    events['pos'] = range(len(events))
    for i, event in events.iterrows():
        letter_tag = chr(97 + event.pos)
        ax.axvline(event.time, color='.15', lw=lw, zorder=-1, ls=ls)

    # date xticks formatting
    ax.xaxis.set_minor_locator(mdates.MonthLocator())
    ax.xaxis.set_major_locator(mdates.YearLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

    # set ylims on sentiment index
    y_min, y_max = np.array(ax.get_ylim())
    diff = y_max - y_min
    if y_min != 0:
        y_min -= diff*.05
    y_max += diff*.05
    ax.set_ylim([y_min, y_max])

    # save
    save_fig(fig, 'heatmap_organisms', version=v)

@paper_plot
def organism_monthly_heatmap():
    df = load_data()
    v = 11
    figsize = (7.2047, 3.5) # double column
    smoothing = 14
    s_date = datetime(2015, 7, 1)
    e_date = datetime(2019, 5, 31)
    plot_organisms = {
            'animals': False,
            'bacteria': False,
            'embryos': True,
            'humans': True,
            'plants': True,
            'not_specified': False
            }
    prominence_cutoff = .2
    lw = .3
    ls = (0, (5, 5))
    min_count_cutoff = 100

    # This is needed for to_pydatetime() conversion (conversion between pandas and matplotlib datetime)
    register_matplotlib_converters()

    # group counts by sentiment, organism and year
    df = df.groupby([df.index.get_level_values('label_sentiment'), df.index.get_level_values('label_organism'), df.index.get_level_values('date').year, df.index.get_level_values('date').month]).sum()

    positive = df.loc['positive']
    negative = df.loc['negative']
    neutral = df.loc['neutral']

    # compute sentiment
    total = positive.add(negative, fill_value=0).add(neutral, fill_value=0)
    sentiment = positive.subtract(negative, fill_value=0).divide(total, fill_value=0)

    # Filter by min_cutoff
    sentiment[total < min_count_cutoff] = np.nan

    # Rearrange columns/index
    sentiment = sentiment.unstack(0)
    sentiment = sentiment.transpose()
    sentiment.rename(index={'not_specified': 'unspecified'}, inplace=True)
    sentiment.sort_index(inplace=True)
    sentiment.columns = sentiment.columns.droplevel(1) # Get rid of "month" column label
    sentiment.index = sentiment.index.droplevel()  # Get rid of counts label


    fig, ax = plt.subplots(1, 1, sharex=True)
    sns.heatmap(sentiment, annot=False, fmt=".2f", cmap='RdYlGn', vmin=-1, vmax=1, ax=ax,
            xticklabels=12, cbar_kws={'label': 'Sentiment'})

    # plot sentiment
    ax.set(ylabel='Organism', xlabel='Year')
    ax.tick_params(axis='both', direction='out')
    ax.collections[0].colorbar.ax.tick_params(axis='y', direction='out')

    # save
    save_fig(fig, 'heatmap_organisms', version=v)

@paper_plot
def organism_monthly_heatmap_v2():
    df = load_data()
    v = 11
    # figsize = (7.2047, 4.4527)   # double column
    figsize = (3.5039, 2.1656) # single column
    min_count_cutoff = 100

    # compute weighted mean and std from daily counts per sentiment
    dft = df.copy()
    dft['year'] = dft.index.get_level_values('date').year
    dft['month'] = dft.index.get_level_values('date').month
    dft.reset_index(inplace=True)
    dft.rename(columns={'label_organism':'organism', 'label_sentiment':'sent'}, inplace=True)
    dft['sent'].replace({'positive':1,'neutral':0,'negative':-1}, inplace=True)
    def stats(df):
        sents = df['sent']
        counts = df['counts']
        mean, sum = np.average(sents, weights=counts, returned=True)
        std = np.sqrt(np.average((sents - mean)**2, weights=counts))
        return pd.DataFrame({'mean': [mean], 'std': [std], 'sum': [sum]})
    dft = dft.groupby(['organism','year','month']).apply(stats)
    dft['sum'] = dft['sum'].astype('int64')
    dft = dft.droplevel(level=3)

    # export monthly mean and std by organism
    dft[dft['sum'] < min_count_cutoff] = np.nan
    dft.drop(columns=['sum'], inplace=True)
    dft.rename(index={'not_specified': 'unspecified'}, inplace=True)
    print(dft.round(2).head())
    save_table(dft, 'organisms_monthly', version=v, round=2)

    # group counts by sentiment, organism and year
    df = df.groupby([df.index.get_level_values('label_sentiment'), df.index.get_level_values('label_organism'), df.index.get_level_values('date').year, df.index.get_level_values('date').month]).sum()

    positive = df.loc['positive']
    negative = df.loc['negative']
    neutral = df.loc['neutral']

    # compute sentiment
    total = positive.add(negative, fill_value=0).add(neutral, fill_value=0)
    sentiment = positive.subtract(negative, fill_value=0).divide(total, fill_value=0)

    # Filter by min_cutoff
    sentiment[total < min_count_cutoff] = np.nan

    # Rearrange columns/index
    sentiment = sentiment.unstack(0)
    sentiment = sentiment.transpose()
    sentiment.rename(index={'not_specified': 'unspecified'}, inplace=True)
    sentiment.sort_index(inplace=True)
    sentiment.columns = sentiment.columns.droplevel(1) # Get rid of "month" column label
    sentiment.index = sentiment.index.droplevel()  # Get rid of counts label

    # get and export overall mean of monthly sentiment means
    organisms = sentiment.agg(['mean','std','count'], axis=1)
    organisms.drop(columns=['count'], inplace=True)
    print(organisms.round(2))
    save_table(organisms, 'organisms_months_means', version=v, round=3)

    # compute total
    total = total.unstack(0)
    total = total.transpose()
    total.rename(index={'not_specified': 'unspecified'}, inplace=True)
    total.sort_index(inplace=True)
    total.columns = total.columns.droplevel(1) # Get rid of "month" column label
    total.index = total.index.droplevel()  # Get rid of counts label

    fig, all_axes = plt.subplots(2, 1, figsize=figsize)
    ax, ax_counts = all_axes
    sns.heatmap(sentiment, annot=False, fmt=".2f", cmap='RdYlBu', vmin=-1, vmax=1, ax=ax, yticklabels=1, xticklabels=12, cbar_kws={'label': 'Sentiment'})
    sns.heatmap(total, annot=False, fmt=".2f", cmap='Blues', vmin=0, ax=ax_counts, yticklabels=1, xticklabels=12, cbar_kws={'label': 'Counts'})

    # Axis labels
    ax.xaxis.label.set_visible(False)
    ax.yaxis.label.set_visible(False)
    ax_counts.yaxis.label.set_visible(False)
    ax_counts.set_xlabel('Year')
    plt.text(-0.07, 0.5, 'Organism', horizontalalignment='center', verticalalignment='center', rotation=90, transform=plt.gcf().transFigure)

    # tick direction
    ax.tick_params(axis='both', direction='out')
    ax.collections[0].colorbar.ax.tick_params(axis='y', direction='out')
    ax.collections[0].colorbar.ax.locator_params(axis='y', nbins=4)
    ax_counts.tick_params(axis='both', direction='out')
    ax_counts.collections[0].colorbar.ax.tick_params(axis='y', direction='out')
    ax_counts.collections[0].colorbar.ax.locator_params(axis='y', nbins=4)

    plt.subplots_adjust(wspace=0, hspace=0.4)

    # plot labels
    label_subplots(all_axes, upper_case=True, offset_points=(-65, 0))

    # save
    save_fig(fig, 'heatmap_organisms', version=v)

if __name__ == "__main__":
    # organism_vs_time()
    # heatmap_with_counts()
    # organism_monthly_heatmap()
    organism_monthly_heatmap_v2()
