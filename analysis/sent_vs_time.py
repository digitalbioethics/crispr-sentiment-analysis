import os
import json
import sys; sys.path.append('..');
from utils.helpers import save_table, save_fig, paper_plot, label_subplots, get_all_data
from preprocess.utils.helpers import find_project_root
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import glob
from datetime import datetime, timedelta
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
import itertools
from scipy.stats import linregress


def load_raw_data():
    df = get_all_data(include_all_data=False)
    df = df[df.p1]
    return df

def get_counts(df, smoothing_days=[1, 7, 14, 21, 28]):
    df_g = df.groupby(['label_sentiment', df.index.date]).count()[['id']]
    df_g.index.rename('date', level=1, inplace=True)
    df_counts = pd.DataFrame(index=df_g.index)
    for s in smoothing_days:
        for agg in ['mean', 'median', 'std']:
            # Compute rolling windows
            df_counts['smooth_{}_{}'.format(s, agg)] = df_g.rolling(s, center=True).agg(agg)
    return df_counts

def load_data():
    cache_path = os.path.join(find_project_root(), 'preprocess', 'data', 'other', 'sent_vs_time_counts.csv')
    if not os.path.isfile(cache_path):
        print('Load data...')
        df = load_raw_data()
        print('Get counts...')
        df = get_counts(df)
        df.to_csv(cache_path)
    print('Reading cached data...')
    df = pd.read_csv(cache_path, index_col=[0,1], parse_dates=['date'])
    return df


def get_events(prominence_cutoff=0, s_date=datetime(2000, 1, 1), e_date=datetime(2020, 1, 1)):
    f_path = os.path.join(find_project_root(), 'other', 'events_meta.json')
    df = pd.read_json(f_path, convert_dates=['time', 'event_time'])
    df = df[df.prominence > prominence_cutoff]
    df = df[(df.time > s_date) & (df.time < e_date)]
    return df

@paper_plot
def sent_vs_time():
    # load data
    df = load_data()
    name = 'sent_vs_time'
    v = 40
    sentiment_smoothing = 7
    activity_smoothing = 7
    s_date = datetime(2015, 7, 1)
    e_date = datetime(2019, 5, 31)
    add_vline_tags = False
    plot_linear_fit = True
    lw = .5
    ls = (0, (5, 5))
    prominence_cutoff = .2
    sentiment_col = 'smooth_{}_{}'.format(1, 'mean')
    activity_col = 'smooth_{}_{}'.format(1, 'mean')
    count_cutoff_sent_index = 20
    tag_events = {
            'human_gene_editing_summit': False,
            'first_human_trial_review': False,
            'first_use_in_humans_china': True,
            'patent_conflict': True,
            'fix_disease_gene_human_embryo': True,
            'mc_gill_study': False,
            'crispr_side_effects_1': True,
            'crispr_babies': True,
            'biohack_malware': True
            }
    tag_all_with_legend = False
    # figsize = (3.5039, 2.1656) # single column
    # figsize = (7.2047, 4.4527) # double column
    figsize = (7.2047, 3.5) # double column

    # This is needed for to_pydatetime() conversion (conversion between pandas and matplotlib datetime)
    register_matplotlib_converters()

    # compute
    total_raw = df.loc['positive'].add(df.loc['negative'], fill_value=0).add(df.loc['neutral'], fill_value=0)
    negative_raw = df.loc['negative']
    positive_raw = df.loc['positive']
    neutral_raw = df.loc['neutral']

    # aggregate
    activity = total_raw.rolling(activity_smoothing, center=True).agg('mean')
    negative_sum = negative_raw.rolling(sentiment_smoothing, center=True).sum()
    positive_sum = positive_raw.rolling(sentiment_smoothing, center=True).sum()
    # neutral_sum = neutral_raw.rolling(sentiment_smoothing, center=True).sum()
    sentiment_index = positive_sum.subtract(negative_sum) / positive_sum.add(negative_sum) # nan-values will become nan values in sent-index!
    sentiment_index[(positive_sum[sentiment_col] < count_cutoff_sent_index) | (negative_sum[sentiment_col] < count_cutoff_sent_index)] = np.nan

    # update s_date and e_date by excluding nans
    s_date_new, e_date_new = sentiment_index[sentiment_col].dropna().index[[0, -1]]
    if s_date_new > s_date:
        s_date = s_date_new
    if e_date_new < e_date:
        e_date = e_date_new

    # select time range and interpolate
    activity = activity[s_date:e_date]
    sentiment_index = sentiment_index[s_date:e_date]
    sentiment_index.interpolate(inplace=True)

    # linear regression
    if plot_linear_fit:
        x = sentiment_index.index.astype(np.int64).values // 10**9   # convert to UNIX time
        y = sentiment_index[sentiment_col].values
        idx = np.isfinite(x) & np.isfinite(y) # only select non-nan values
        p, V = np.polyfit(x[idx], y[idx], 1, cov=True)
        slope, intercept, r_value, p_value, std_err = linregress(x[idx], y[idx])
        lin_reg = np.poly1d(p)
        linear_fit = lin_reg(x)
        p[0] *= 3600*24*365.25  # convert from seconds to year
        V[0][0] *= 3600*24*365.25  # convert from seconds to year
        slope *= 3600*24*365.25  # convert from seconds to year
        std_err *= 3600*24*365.25  # convert from seconds to year
        print('Linear fit results:')
        print("Slope: {:.2E} +/- {:.2E}".format(p[0], np.sqrt(V[0][0])))
        print("Intercept: {:.2E} +/- {:.2E}".format(p[1], np.sqrt(V[1][1])))
        print("Slope with SE: {:.3E} +/- {:.3E}".format(slope, std_err))

    # plot
    fig, all_axes = plt.subplots(2, 1, sharex=True, figsize=figsize)
    ax_sentiment_index, ax_activity  = all_axes
    ax_sentiment_index.plot(sentiment_index.index.to_pydatetime(), sentiment_index[sentiment_col].values, label='Sentiment index', zorder=1)
    ax_activity.fill_between(activity.index.to_pydatetime(), activity[activity_col].values, label='Activity', zorder=0)

    # plot linear regression
    if plot_linear_fit:
        ax_sentiment_index.plot(sentiment_index.index.to_pydatetime(), linear_fit, zorder=0)

    # add events
    events = get_events(prominence_cutoff=prominence_cutoff, s_date=s_date, e_date=e_date)
    axvlines_artists = []
    events['pos'] = range(len(events))
    for i, event in events.iterrows():
        letter_tag = chr(97 + event.pos)
        l = ax_activity.axvline(event.time, color='.15', lw=lw, zorder=-1, ls=ls, label='{}) {}'.format(letter_tag, event.key))
        ax_sentiment_index.axvline(event.time, color='.15', lw=lw, zorder=-1, ls=ls)
        if tag_all_with_legend:
            axvlines_artists.append(l)
            ax_activity.annotate(letter_tag, xy=(event.time, .85), xytext=(-4, 0), xycoords=('data', 'axes fraction'), textcoords='offset pixels', horizontalalignment='right')
    if tag_all_with_legend:
        lg1 = plt.legend(handles=axvlines_artists, bbox_to_anchor=(1, 1.7), fontsize=4)

    # add tags
    if not tag_all_with_legend:
        c = 0
        for key, tag_event in tag_events.items():
            if tag_event:
                offset = timedelta(days=7)
                ax_activity.annotate(chr(97 + c), xy=(events[events.key == key].time - offset, 0.98), xycoords=('data', 'axes fraction'), verticalalignment='top', horizontalalignment='right')
                c += 1

    # set ylims on sentiment index
    for ax in all_axes:
        y_min, y_max = np.array(ax.get_ylim())
        diff = y_max - y_min
        if y_min != 0:
            y_min -= diff*.05
        y_max += diff*.05
        ax.set_ylim([y_min, y_max])

    # date xticks formatting
    axis_with_year_ticklabels(ax_activity)

    # axis labels
    ax_sentiment_index.set_ylabel('Sentiment index $s$')
    ax_activity.set_ylabel('Daily counts')
    ax_activity.xaxis.label.set_visible(False)

    # remove space between plots
    plt.subplots_adjust(wspace=0, hspace=0.1)

    # remove xticks of upper plot
    # ax_sentiment_index.xaxis.set_ticks_position('none')

    # Horizontal lines
    ax_sentiment_index.axhline(0, color='.8', lw=.3, zorder=-1)
    ax_sentiment_index.axhline(1, color='.8', lw=.3, zorder=-1)

    # tick direction
    ax_activity.tick_params(axis='x', direction='out', which='minor', zorder=2, pad=6, size=2)
    ax_activity.tick_params(axis='x', direction='out', which='major', zorder=2, size=5)
    ax_sentiment_index.tick_params(axis='x', which='major', size=5)
    ax_sentiment_index.tick_params(axis='x', which='minor', size=2)
    ax_activity.spines['bottom'].set_zorder(2)

    # plot labels
    label_subplots(all_axes, upper_case=True)

    # save
    save_fig(fig, name, version=v)


def axis_with_year_ticklabels(ax):
    # set minor ticks as months and major as years
    ax.xaxis.set_minor_locator(mdates.MonthLocator())
    ax.xaxis.set_major_locator(mdates.YearLocator())
    # hacky way to center year labels between major ticks
    plt.setp(ax.get_xmajorticklabels(), visible=False)
    ax.xaxis.set_minor_formatter(mdates.DateFormatter('%Y'))
    plt.gcf().canvas.draw()
    labels = [l.get_text() for l in ax.get_xminorticklabels()]
    for i, l in enumerate(labels):
        if (i-5) % 11 - 6 != 0:
            labels[i] = ''
        else:
            labels[i] = labels[i]
    ax.set_xticklabels(labels, minor=True)


if __name__ == "__main__":
    sent_vs_time()
