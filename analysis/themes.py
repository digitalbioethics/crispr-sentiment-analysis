import sys; sys.path.append('..')
import os
import re
from preprocess.utils.helpers import find_folder
from utils.helpers import save_table, save_fig, paper_plot, label_subplots, get_all_data
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

THEMES = {
    'disease': 'diseases?',
    'health': 'restore',
    'therapy': 'therapy|therapeutic',
    'germline': 'germline|heritable|stem[\s-]cell|heritage',
    'somatic': 'somatic',
    'enhancement': 'enhanc(e|ement|ing)',
    'improvement': 'improv(e|ement|ing)',
    'treatment': 'treat(ment|ing)?',
    'reducing': '(lower(ing)?|reduc(e|ing))\s.*risk',
    'prevention': 'prevent(ion|ing)?',
    'risk': 'risks?',
    'cure': 'cur(e|ing)',
    'progress': 'scientific progress',
    'traits': 'traits?',
    'abilities': 'abilit(y|ies)',
    'intelligence': 'intelligence',
    'appearance': 'appearance',
    'price': 'expensive',
    'discovery': 'discovery?|anticipat(e|ion)',
    'privacy': 'privacy',
    'accuracy': 'accuracy',
    'reliability': 'reliability',
    'mutation': 'mutations?',
    'eugenic': 'eugenic',
    'trust': 'trust',
    'children': 'child(ren)?',
    'genome': 'genome|genomics?|genes?|genetic',
    'embryo': 'embryo(nic)?',
    'baby': 'bab(y|ies)'
}

def load_raw_data():
    df = get_all_data(include_all_data=True)
    df = df[df.p1]
    return df

def load_data(from_cache=True):
    cache_path = os.path.join(find_folder('other'), 'themes.csv')
    if not os.path.isfile(cache_path) or not from_cache:
        print('Load data...')
        df = load_raw_data()
        # regex representing themes of positive/negative attitudes from studies
        print('Compute themes...')
        # add helper columns with bool if regex matches text
        for key, regex in THEMES.items():
            df[key] = df.text.str.contains(regex)
        # adding year and sentiment helper columns
        df['year'] = df.index.year
        df['sentiment'] = df['label_sentiment']
        df.reset_index()
        columns = list(THEMES.keys()) + ['year', 'sentiment']
        df = df[columns]
        df.to_csv(cache_path)
    print('Reading cached data...')
    df = pd.read_csv(cache_path, dtype={'year': str}, index_col=0)
    return df


@paper_plot
def themes():
    # vars
    n = 6
    v = 4
    # figsize = (3.5039, 2.1656) # single column
    figsize = (7.2047, 2.3) # double column

    # load data
    df = load_data(from_cache=True)
    length = len(df)

    # group and select theme columns, then sum
    df = df.groupby(['sentiment','year']).sum()
    dfr = df.copy()

    # extrapolate numbers for 2019 and keep copy of 2019
    df2019 = df[df.index.get_level_values('year') == '2019'].copy()
    df[df.index.get_level_values('year') == '2019'] = (df2019 / 5 * 12)
    df.rename(index={'2019': '2019*'}, inplace=True)
    df = df.round()

    # get n largest themes
    top_keys = df.sum().nlargest(n=n).index
    print("Raw counts:\n{} / {}".format(dfr[top_keys].sum(), length))
    print("Extrapolated counts:\n{} / {}".format(df[top_keys].sum(), length))

    # save counts per sentiment and year for top n themes, and raw numbers for 2019
    dfs = df.append(df2019)
    print(dfs[top_keys].head())
    save_table(dfs[top_keys].astype('int64'), 'themes_sentiment_yearly_top_{}'.format(n), version=v)
    # save_table(df2019[top_keys].astype('int64'), 'themes_sentiment_yearly_top_{}_2019'.format(n), version=v)

    # plot n largest themes by sentiment
    fig, all_axes = plt.subplots(1, 3, figsize=figsize, sharex=True)

    for (sentiment, df_sentiment), ax in zip(df.groupby(level='sentiment'), all_axes):
        df_sentiment[top_keys].droplevel('sentiment').plot(ax=ax, legend=False)
        ax.set_title(sentiment.capitalize())
        ax.xaxis.label.set_visible(False)
        ax.locator_params(axis='y', nbins=5)
        loc = MultipleLocator(base=1.0)
        ax.xaxis.set_major_locator(loc)
        ax.tick_params(axis='x', which='major', direction='out')
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(5)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(5)

        # set ylims
        y_min, y_max = np.array(ax.get_ylim())
        diff = y_max - y_min
        if y_min != 0:
            y_min -= diff*.05
        y_max += diff*.05
        ax.set_ylim([0, y_max])

    all_axes[0].set_ylabel('Yearly counts')
    all_axes[-1].legend(bbox_to_anchor=(1.05, 1), borderaxespad=0)

    # plot labesl
    label_subplots(all_axes, upper_case=True, offset_points=(-10, 20))

    # save figure
    save_fig(fig, 'themes', version=v)


if __name__ == "__main__":
    themes()
