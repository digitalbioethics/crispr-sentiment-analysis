import os
import json
import sys; sys.path.append('..');
from utils.helpers import save_table, save_fig, paper_plot, label_subplots, get_all_data
from preprocess.utils.helpers import find_project_root
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import glob
from datetime import datetime, timedelta
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
import itertools
from scipy.stats import ttest_ind
from tqdm import tqdm

version = 3
round_m = 2
round_p = 4
round_t = 2

def load_raw_data():
    df = get_all_data(include_all_data=False)
    df = df[df.p1]
    return df

df = None
def load_data():
    global df
    if df is None:
        print('loading raw data...')
        df = load_raw_data()
        df['year'] = df.index.year
        df['sent'] = df['label_sentiment'].replace({'positive':1,'neutral':0,'negative':-1})
        df['organism'] = df['label_organism'].replace({'not_specified':'unspecified'})
    return df.copy()

def bootstrap_estimate(t, x1, x2, N=1000, minsize=True, l=20000):
    m1 = x1.mean()
    m2 = x2.mean()
    mt = x1.append(x2).mean()
    xn1 = x1 - m1 + mt
    xn2 = x2 - m2 + mt
    xn1.dropna(inplace=True)
    xn2.dropna(inplace=True)
    l1 = len(xn1)
    l2 = len(xn2)
    if minsize:
        l1 = min(l1, l2)
        l2 = min(l1, l2)
    if l:
        l1 = min(l1, l)
        l2 = min(l2, l)
    ts = []
    for i in range(N):
        s1 = xn1.sample(n=l1, replace=True)
        s2 = xn2.sample(n=l2, replace=True)
        tstats, pvalue = ttest_ind(s1, s2, nan_policy='omit', equal_var=False)
        ts.append(tstats)
    ts = np.array(ts)
    c = len(ts[np.where(abs(ts) >= abs(t))])
    return (c + 1) / (N + 1)


def yearly_sentiment_diff_significance(neutral=False, t=True, p=True, bootstrap=0):
    stats = load_data()
    # calculate statistics and compare them
    print("\n** year statistics **")
    if not neutral:
        stats.loc[stats['sent']==0,'sent'] = np.nan
    # stats = stats[datetime(2015, 7, 1):]
    years = stats.groupby('year')['sent'].agg(['mean','std','count'])
    for iy in years.index:
        if p:
            years['p-'+str(iy)] = np.nan
        if bootstrap:
            years['bp-'+str(iy)] = np.nan
    for iy in tqdm(years.index):
        if t:
            years['t-'+str(iy)] = np.nan
        for jy in years.index:
            if iy < jy:
                continue
            x1 = stats[stats['year']==iy]['sent']
            x2 = stats[stats['year']==jy]['sent']
            tstats, pvalue = ttest_ind(x1, x2, nan_policy='omit', equal_var=False)
            if t:
                years.loc[jy,'t-'+str(iy)] = round(tstats, round_t)
            if p:
                years.loc[jy,'p-'+str(iy)] = round(pvalue, round_p)
            if bootstrap:
                bp = bootstrap_estimate(tstats, x1, x2, bootstrap)
                years.loc[jy,'bp-'+str(iy)] = round(bp, round_p)
    years = years.round({'mean': round_m, 'std': round_m})
    print('Mean difference significance (Welch\'s t test two-sided p-values):')
    print(years)
    save_table(years, 'years_stats', version=version)


def organism_sentiment_diff_significance(t=True, p=True, bootstrap=0):
    stats = load_data()
    # calculate statistics and compare them
    print("\n** organism statistics **")
    organisms = stats.groupby('organism')['sent'].agg(['mean','std','count'])
    for io in organisms.index:
        if p:
            organisms['p-'+str(io)] = np.nan
        if bootstrap:
            organisms['bp-'+str(io)] = np.nan
    for io in tqdm(organisms.index):
        if t:
            organisms['t-'+str(io)] = np.nan
        for jo in organisms.index:
            if io < jo:
                continue
            x1 = stats[stats['organism']==io]['sent']
            x2 = stats[stats['organism']==jo]['sent']
            tstats, pvalue = ttest_ind(x1, x2, nan_policy='omit', equal_var=False)
            if t:
                organisms.loc[jo,'t-'+str(io)] = round(tstats, round_t)
            if p:
                organisms.loc[jo,'p-'+str(io)] = round(pvalue, round_p)
            if bootstrap:
                bp = bootstrap_estimate(tstats, x1, x2, bootstrap)
                organisms.loc[jo,'bp-'+str(io)] = round(bp, round_p)
    organisms = organisms.round({'mean': round_m, 'std': round_m})
    print('Mean difference significance (Welch\'s t test two-sided p-values):')
    print(organisms)
    save_table(organisms, 'organisms_stats', version=version)


if __name__ == "__main__":
    yearly_sentiment_diff_significance()
    organism_sentiment_diff_significance()
    print("\ndone.")
