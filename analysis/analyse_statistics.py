import sys; sys.path.append('..');
from utils.helpers import find_file, find_folder
from utils.analysis.helpers import save_fig
import pandas as pd
import numpy as np
import csv
import os
import matplotlib.pyplot as plt


class AnalyseStatistics(object):
    """Wrapper class for functions to statistically analyse tweets/predictions"""

    def __init__(self):
        pass

    def use_data(self, df, sample_size = None):
        # use subset of data if sample_size is specified
        if not sample_size is None and sample_size < len(df):
            df = df.loc[df.is_retweet==False].sample(n=sample_size)
        # add helper columns for grouping
        df['datetime'] = df.index.to_series()
        df['year'] = [date.year for date in df.datetime]
        df['month'] = [date.month for date in df.datetime]
        df['day'] = [date.day for date in df.datetime]
        # add columns with preprocessed values
        df['entities.hashtags'] = df['entities.hashtags'].replace(np.nan, '', regex=True)
        df['hashtags'] = df['entities.hashtags'].str.replace('[\[\]\']','').str.split(',')
        df['text_pp'] = df.text.str.lower().str.replace('\<url\>|\<user\>','').str.replace('\s+',' ').str.replace('[^a-z\s]', '')
        df['unigrams'] = [[w for w in str(text).split(' ') if len(w)>3] for text in df.text_pp]
        df['bigrams'] = [[unigrams[i-1] + ' ' + unigrams[i] for i in range(1,len(unigrams))] for unigrams in df.unigrams]
        df['trigrams'] = [[unigrams[i-2] + ' ' + unigrams[i-1] + ' ' + unigrams[i] for i in range(2,len(unigrams))] for unigrams in df.unigrams]
        # set member df for use in other methods
        self.df = df
        # return for use outside class
        return df

    def rank(self, df = None, key = 'label_sentiment', normalize = True, export = True, groupby = None, filterby = None, exclude = None):
        if df is None:
            df = self.df
        columns = [key,'id','year','month','day']
        if not groupby is None:
            columns.append(groupby)
        if not filterby is None:
            df = df.loc[filterby]
        print("processing " + str(len(df)) + " rows")
        if len(df) > 0 and isinstance(df[key][0], pd.core.series.Series):
            value_list = [[value.lower().strip(), *row[1:]] for index, row in df[columns].iterrows() for value in row[0] if value!='']
            ldf = pd.DataFrame(value_list, columns=columns)
        else:
            ldf = df[columns].copy()
        if not exclude is None:
            ldf['exclude'] = [x in exclude for x in ldf[key]]
            ldf = ldf.loc[~ldf.exclude]
        groupcols = ['year']
        if not groupby is None:
            groupcols.append(groupby)
        lg = ldf.groupby(groupcols)[key].value_counts(normalize=normalize)
        if export:
            gdf = lg.rename(columns={key:'key'}).reset_index()
            gdf = gdf.rename(columns={0:'count'})
            if not groupby is None:
                for group in gdf[groupby].unique():
                    gdfg = gdf.loc[gdf[groupby]==group].copy()
                    self.save_for_rankflow(key, gdfg, group = group, normalized = normalize)
                    self.save_table(key, gdfg, group = group)
            else:
                self.save_for_rankflow(key, gdf, normalized = normalize)
                self.save_table(key, gdf)
        return lg

    def save_table(self, key, gdf, group = None):
        data_folder = find_folder('7_analysis')
        suffix = "";
        if not group is None:
            suffix = "_" + str(group)
        path = os.path.join(data_folder, "table_" + key.replace('.','_') + suffix + ".csv")
        gdf.to_csv(path)

    def save_for_rankflow(self, key, gdf, n = 20, years = None, group = None, normalized = False):
        if years is None:
            years = list(gdf['year'].unique())
            years.sort()
        if normalized:
            gdf['count'] = gdf['count'] * 1000
        topn = [gdf.loc[gdf.year==year].sort_values('count', ascending=False).iloc[0:n][[key,'count']].values for year in years]
        labels = []
        for year in years:
            labels.extend([str(year) + '_' + key,str(year) + '_count'])
        output = [labels]
        for i in range(0,n):
            row = []
            for j in range(0,len(years)):
                if i<len(topn[j]):
                    row.extend(topn[j][i])
                else:
                    row.extend(['none',0])
            output.append(row)
        data_folder = find_folder('7_analysis')
        suffix = "";
        if not group is None:
            suffix = "_" + str(group)
        path = os.path.join(data_folder, "rf_" + key.replace('.','_') + suffix + ".csv")
        with open(path, "w") as f:
            writer = csv.writer(f, delimiter='\t')
            writer.writerows(output)

    def metrics(self, metrics, df = None, interval = 'year', export = True):
        if df is None:
            df = self.df
        for key,regex in metrics.items():
            df[key] = df.text.str.contains(regex)
        counts = {}
        columns = metrics.keys()
        for col in columns:
            counts[col] = []
        if isinstance(interval, list):
            intervals = interval
            interval = "_".join(interval)
            df[interval] = ["-".join(str(i) for i in x) for x in df[intervals].values]
        bins = list(df[interval].unique())
        bins.sort()
        for bin in bins:
            for key in metrics.keys():
                counts[key].append(len(df.loc[(df[key]==True)&(df[interval]==bin)]))
        kdf = pd.DataFrame(counts, index=bins)
        if export:
            self.save_table('topics_' + interval, kdf)
        kdf.plot.line()
        return kdf
