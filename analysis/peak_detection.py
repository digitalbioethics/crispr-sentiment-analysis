import os
import sys; sys.path.append('..');
from utils.helpers import save_fig, notebook_plot, find_project_root
from preprocess.utils.helpers import find_folder
from utils.helpers import get_all_data
import scipy.signal
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
import matplotlib.dates as mdates
from other.events import EVENTS
import pandas as pd
from datetime import datetime, timedelta
import numpy as np
import json
from preprocess.utils.misc import JSONEncoder
from collections import defaultdict
import pickle
import re
import requests

def load_raw_data(include_all_data=False, dtype='anonymized'):
    df = get_all_data(include_all_data=include_all_data, dtype=dtype)
    df = df[df.p1]
    return df

def get_counts(df, smoothing_days=[1, 7, 14, 21, 28]):
    df_g = df.groupby(['label_sentiment', df.index.date]).count()[['id']]
    df_g.index.rename('date', level=1, inplace=True)
    df_counts = pd.DataFrame(index=df_g.index)
    for s in smoothing_days:
        for agg in ['mean', 'median', 'std']:
            # Compute rolling windows
            df_counts['smooth_{}_{}'.format(s, agg)] = df_g.rolling(s, center=True).agg(agg)
    return df_counts

def load_data():
    cache_path = os.path.join(find_folder('other'), 'sent_vs_time_counts.csv')
    if not os.path.isfile(cache_path):
        print('Generate sentiment data...')
        df = load_raw_data()
        df = get_counts(df)
        df.to_csv(cache_path)
    print('Reading cached sentiment data...')
    df = pd.read_csv(cache_path, index_col=[0,1], parse_dates=['date'])
    return df

@notebook_plot
def find_peaks_by_prominence():
    # This is needed for to_pydatetime() conversion (conversion between pandas and matplotlib datetime)
    register_matplotlib_converters()

    # vars
    name = 'peak_detection'
    v = 1
    s_date = datetime(2013, 1, 1)
    e_date = datetime(2019, 5, 31)
    smooth_days = 7    # smoothing window
    agg_fun = 'mean' # aggregation function
    lw = .3

    # load data and compute activity
    df = load_data()
    activity = df.loc['positive'].add(df.loc['negative'], fill_value=0).add(df.loc['neutral'], fill_value=0)
    activity /= activity.loc[s_date:e_date].max()

    # select date range & smoothing
    activity = activity[s_date:e_date]['smooth_{}_{}'.format(smooth_days, agg_fun)]

    # plot activity
    n_rows = 3
    n_cols = 3
    fig, axes = plt.subplots(n_rows, n_cols, sharex=True, sharey=True, figsize=(8, 6))
    prominence = np.linspace(0, .2, n_rows*n_cols)
    c = 0
    for i, row in enumerate(axes):
        for j, ax in enumerate(row):
            # detect peaks
            peak_indices, _ = scipy.signal.find_peaks(activity, prominence=prominence[c])
            peak_dates = activity.iloc[peak_indices].index

            # plot activity
            ax.fill_between(activity.index.to_pydatetime(), activity.values, label='Normalized activity', zorder=1)

            # plot peaks
            for peak_date in peak_dates:
                ax.axvline(peak_date, c='k', lw=lw, zorder=0)

            ax.set_title('Prominence: {}'.format(prominence[c]))

            # date xticks formatting
            ax.xaxis.set_major_locator(mdates.YearLocator())
            ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

            c += 1

    plt.tight_layout()

    # save
    save_fig(fig, name, version=v)

@notebook_plot
def compare_to_events():
    # This is needed for to_pydatetime() conversion (conversion between pandas and matplotlib datetime)
    register_matplotlib_converters()

    # vars
    name = 'peak_detection'
    v = 4
    s_date = datetime(2015, 1, 1)
    e_date = datetime(2019, 5, 31)
    # s_date = datetime(2018, 1, 15)
    # e_date = datetime(2018, 1, 30)
    smooth_days = 7    # smoothing window
    agg_fun = 'mean' # aggregation function
    lw = .3
    prominence = 0.1
    ls = (0, (5, 5))

    # load data and compute activity
    df = load_data()
    activity = df.loc['positive'].add(df.loc['negative'], fill_value=0).add(df.loc['neutral'], fill_value=0)
    activity /= activity.loc[s_date:e_date].max()

    # select date range & smoothing
    activity = activity[s_date:e_date]['smooth_{}_{}'.format(smooth_days, agg_fun)]

    # plot activity
    n_rows = 1
    n_cols = 1
    fig, ax = plt.subplots(n_rows, n_cols, sharex=True, sharey=True)

    # detect peaks
    peak_indices, stats = scipy.signal.find_peaks(activity, prominence=prominence)
    peak_dates = activity.iloc[peak_indices].index

    # plot activity
    ax.fill_between(activity.index.to_pydatetime(), activity.values, zorder=1)

    # plot peaks
    peak_axes = []
    for peak_date in peak_dates:
        if peak_date > s_date and peak_date < e_date:
            peak_ax = ax.axvline(peak_date, c='k', ls=ls, lw=lw, zorder=0, label=peak_date.strftime('%Y-%m-%d'))
            peak_axes.append(peak_ax)

    # plot events
    ev_axes = []
    for event in EVENTS:
        if event['time'] > s_date and event['time'] < e_date:
            if event['visible']:
                ev_ax = ax.axvline(event['time'], c='red', lw=lw, zorder=0, label=event['time'].strftime('%Y-%m-%d'))
                ev_axes.append(ev_ax)


    # date xticks formatting
    ax.xaxis.set_major_locator(mdates.YearLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

    ax.set_title('Prominence: {}'.format(prominence))

    lg1 = ax.legend(handles=peak_axes, bbox_to_anchor=(1, 1), fontsize=4, title='Detected peaks')
    plt.setp(lg1.get_title(),fontsize=6)
    ax.add_artist(lg1)
    lg2 = ax.legend(handles=ev_axes, bbox_to_anchor=(1.51, 1), fontsize=4, title='Already added')
    plt.setp(lg2.get_title(),fontsize=6)

    # save
    save_fig(fig, name, version=v)

def unfold_url(url):
    session = requests.Session()  # so connections are recycled
    resp = session.head(url, allow_redirects=True)
    return resp.url

def get_urls(text):
    l = re.findall('((www\.[^\s]+)|(https?://[^\s]+)|(http?://[^\s]+))', text)
    urls = [unfold_url(_l[0]) for _l in l]
    return [url for url in urls if 'twitter' not in url]

if __name__ == "__main__":
    find_peaks_by_prominence()
    compare_to_events()
