import sys; sys.path.append('..')
import os
import re
from preprocess.utils.helpers import find_folder
from utils.helpers import save_table, save_fig, paper_plot, get_all_data
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.mlab as mlab
from matplotlib.path import Path
from matplotlib.patches import Rectangle, PathPatch
from matplotlib.colors import Normalize
from matplotlib.colorbar import ColorbarBase
from matplotlib.cm import ScalarMappable

def load_raw_data():
    df = get_all_data(include_all_data=True)
    df = df[df.p1]
    return df

def load_data(from_cache=True):
    cache_path = os.path.join(find_folder('other'), 'rankflow_hashtag_sentiment.csv')
    if not os.path.isfile(cache_path) or not from_cache:
        print('Load data...')
        df = load_raw_data()
        print('Compute hashtags and sentiment...')
        df['sentiment'] = df['label_sentiment']
        df['numsent'] = [1 if label == 'positive' else -1 if label == 'negative' else 0 for label in df['label_sentiment']]
        df['hashtag'] = [ast.literal_eval(row['entities.hashtags']) if pd.notna(row['entities.hashtags']) else re.findall('#(\w+)', row['text']) for index, row in df.iterrows()]
        df['hashtag'] = df['hashtag'].apply(lambda s: [_s.lower().strip() for _s in s])
        df['year'] = df.index.year
        df.reset_index(drop=True, inplace=True)
        df = df[['hashtag', 'sentiment', 'numsent', 'year']]
        df = df.explode('hashtag')
        df.dropna(subset=['hashtag'], inplace=True)
        df.to_csv(cache_path)
    print('Reading cached data...')
    df = pd.read_csv(cache_path, index_col=0)
    return df

@paper_plot
def rankflow_hashtags(include_crispr=False, normalize=False):
    def get_color(value):
        norm = Normalize(vmin=-1,vmax=1)
        f_cmap = cm.get_cmap(cmap)
        return f_cmap(norm(value))

    def get_rect(i, year, df, minheight=200, gapheight=2000):
        rows = df[df.year==year]
        diff = len(rows) - i;
        count = 0
        for j in range(i+1,i+diff):
            count += rows.iloc[j]['count'] + minheight
        return Rectangle((year - 0.05, count + gapheight*diff), 0.1, rows.iloc[i]['count'] + minheight, fc=get_color(rows.iloc[i]['mean'])), rows.iloc[i]

    def connect_rects(left, right, rowl=None, rowr=None):
        lefttop = (left.get_x() + left.get_width(), left.get_y() + left.get_height())
        righttop = (right.get_x(), right.get_y() + right.get_height())
        topinter1 = (lefttop[0] + (righttop[0]-lefttop[0])*0.5, lefttop[1])
        topinter2 = (lefttop[0] + (righttop[0]-lefttop[0])*0.5, righttop[1])
        leftbottom = (left.get_x() + left.get_width(), left.get_y())
        rightbottom = (right.get_x(), right.get_y())
        bottominter1 = (leftbottom[0] + (rightbottom[0]-leftbottom[0])*0.5, leftbottom[1])
        bottominter2 = (leftbottom[0] + (rightbottom[0]-leftbottom[0])*0.5, rightbottom[1])
        return PathPatch(
        Path([lefttop, topinter1, topinter2, righttop, rightbottom, bottominter2, bottominter1, leftbottom, lefttop],
            [Path.MOVETO, Path.CURVE4, Path.CURVE4, Path.CURVE4, Path.LINETO, Path.CURVE4, Path.CURVE4, Path.CURVE4, Path.CLOSEPOLY]),
            fc='.8', ec='white', lw=0.3, alpha=.3, zorder=-1)

    # vars
    n = 15
    v = 5
    cmap = 'RdYlBu'
    # figsize = (3.5039, 2.1656) # single column
    figsize = (7.2047, 4.4527) # double column
    text_fmt = {
            'horizontalalignment': 'left',
            'verticalalignment': 'center',
            'fontsize': 4
            }

    df = load_data()
    if not include_crispr:
        df = df[df.hashtag != 'crispr']
    df = df.groupby(['year', 'hashtag']).agg(count=('numsent', 'count'), mean=('numsent', 'mean'), std=('numsent', 'std'))
    df = df.groupby(level='year').apply(lambda grp: grp.nlargest(n, columns=['count']))
    df = df.droplevel(level=1)

    # extrapolate numbers for 2019
    idx2019 = df.index.get_level_values('year') == 2019
    df2019 = df.loc[idx2019].copy()
    df.loc[idx2019, 'count'] = (df2019['count'] / 5 * 12).round()
    df['count'] = df['count'].astype('int64')

    # save table before resetting index
    dfs = df.copy()
    dfs.rename(index={2019: '2019*'}, inplace=True)
    dfs = dfs.append(df2019)
    dfs['count'] = dfs['count'].astype('int64')
    print(dfs.round(2).head());
    save_table(dfs, 'rankflow_hashtags_top_{}'.format(n), version=v, round=2)

    df = df.reset_index()
    df['i'] = df.groupby('year').cumcount()
    fig, ax = plt.subplots(figsize=figsize)
    lines = {}
    top = 0

    for year in range(2013,2020):
        for i in range(0, len(df[df.year==year])):
            rect, row = get_rect(i, year, df)
            ax.add_patch(rect)
            hashtag = row.hashtag
            if not hashtag in lines:
                lines[hashtag] = []
            lines[hashtag].append(rect)
            ax.text(rect.get_x() + rect.get_width() + 0.05, rect.get_y() + rect.get_height() / 2, row.hashtag + " (" + str(row['count']) + ")", **text_fmt)
            top = max(top, rect.get_y() + rect.get_height())

    for hashtag in lines:
        rects = lines[hashtag]
        length = len(rects)
        if length > 1:
            for i in range(1,length):
                patch = connect_rects(rects[i-1],rects[i])
                ax.add_patch(patch)

    ax.set(xlim=(2012.8, 2020), ylim=(0,top + 2000))
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_yaxis().set_ticks([])
    xlabels = list([str(i) for i in range(2013, 2020)])
    xlabels[-1] = '2019*'
    ax.get_xaxis().set_ticks(range(2013, 2020))
    ax.set_xticklabels(xlabels)

    # colorbar
    norm = Normalize(vmin=-1, vmax=1)
    cax = fig.add_axes([0.15, .84, 0.2, 0.01])
    cbar = fig.colorbar(ScalarMappable(norm=norm, cmap=cmap), ax=ax, cax=cax, orientation='horizontal', shrink=1)
    cbar.ax.tick_params(axis='x', direction='out')
    cbar.set_label('Sentiment')
    cbar.outline.set_visible(False)

    save_fig(fig, 'hashtag_rankflow', plot_formats=['png', 'pdf'], version=v)

if __name__ == "__main__":
    rankflow_hashtags()
