import os
from functools import wraps
import matplotlib.pyplot as plt
from preprocess.utils.helpers import get_cleaned_data, get_uploaded_batched_data, get_labelled_data, get_cleaned_labelled_data, get_predicted_data
import pandas as pd


def find_project_root(num_par_dirs=8):
    for i in range(num_par_dirs):
        par_dirs = i*['..']
        current_dir = os.path.join(*par_dirs, '.git')
        if os.path.isdir(current_dir):
            break
    else:
        raise FileNotFoundError('Could not find project root folder.')
    return os.path.join(*os.path.split(current_dir)[:-1])

def save_fig(fig, name, plot_formats=['png', 'eps'], version=1):
    def f_name(fmt):
        f_name = '{}.{}'.format(name, fmt)
        return os.path.join(folder_path, f_name)
    folder_path = os.path.join(find_project_root(), 'analysis', 'plots', name, 'v{}'.format(version))
    if not os.path.isdir(folder_path):
        os.makedirs(folder_path)
    for fmt in plot_formats:
        if not fmt == 'tiff':
            f_path = f_name(fmt)
            print('Writing figure file {}'.format(os.path.abspath(f_path)))
            fig.savefig(f_name(fmt), bbox_inches='tight')
    if 'tiff' in plot_formats:
        os.system("convert {} {}".format(f_name('png'), f_name('tiff')))

def save_table(df, name, version=1, round=None, table_formats=['csv','tex']):
    folder_path = os.path.join(find_project_root(), 'analysis', 'tables', name, 'v{}'.format(version))
    if not os.path.isdir(folder_path):
        os.makedirs(folder_path)
    if not round is None:
        df = df.round(round)
    for fmt in table_formats:
        f_path = os.path.join(folder_path, '{}.{}'.format(name, fmt))
        print('Writing table file {}'.format(os.path.abspath(f_path)))
        if fmt == 'csv':
            df.to_csv(f_path)
        elif fmt == 'tex':
            df.to_latex(f_path)

    # tex = ""
    # headers = df.reset_index().columns
    # if df.index.nlevels > 1:
    #     for idx in df.index.get_level_values(0):
    #         tex


def use_style_sheet(name='notebook'):
    style_path = os.path.join(find_project_root(), 'other', 'stylesheets', '{}.mplstyle'.format(name))
    plt.style.use(style_path)

def notebook_plot(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        style_sheet = os.path.join(find_project_root(), 'other', 'stylesheets', 'notebook.mplstyle')
        plt.style.use(style_sheet)
        return func(*args, **kwargs)
    return wrapper

def paper_plot(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        style_sheet = os.path.join(find_project_root(), 'other', 'stylesheets', 'paper.mplstyle')
        plt.style.use(style_sheet)
        return func(*args, **kwargs)
    return wrapper

def paper_single(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        style_sheet = os.path.join(find_project_root(), 'other', 'stylesheets', 'paper_single_column.mplstyle')
        plt.style.use(style_sheet)
        return func(*args, **kwargs)
    return wrapper

def paper_double(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        style_sheet = os.path.join(find_project_root(), 'other', 'stylesheets', 'paper_double_column.mplstyle')
        plt.style.use(style_sheet)
        return func(*args, **kwargs)
    return wrapper

def use_stylesheet(name='paper'):
    style_sheet = os.path.join(find_project_root(), 'other', 'stylesheets', '{}.mplstyle'.format(name))
    plt.style.use(style_sheet)

def label_subplots(axes, upper_case=True, offset_points=(-40, 0)):
    start_ord = 65 if upper_case else 97
    for ax, lab in zip(axes, ['{}'.format(chr(j)) for j in range(start_ord, start_ord + len(axes))]):
        ax.annotate(lab, (0, 1), xytext=offset_points, xycoords='axes fraction', textcoords='offset points',
                ha='right', va='top', weight='bold')

def get_all_data(include_all_data=True, dtype='anonymized', token_count_cutoff=3, s_date='2013-01-01', e_date='2019-05-31', mode='mturk',
        cleaned_labels_name_1='cleaned_labels_cutoff-worker-outliers-3.0_min-labels-cutoff-3_unanimous',
        cleaned_labels_name_2='cleaned_labels_is-relevant_cutoff-worker-outliers-3.0_min-labels-cutoff-3_unanimous',
        include_predictions=True):
    """
    Returns all data including predictions and filters
    :param include_all_data: If set to False return the minimal possible number of columns (id, predictions, filters), default: True
    :param dtype: Raw data dtype (default: anonymized)
    :param s_date: Start date filter
    :param e_date: End date filter
    :param token_count_cutoff: Word token cut-off (default: 3)
    :param cleaned_labels_name_1: Cleaned label filter 1
    :param cleaned_labels_name_2: Cleaned label filter 2
    """
    # load data
    if include_all_data:
        usecols = None
    else:
        usecols = ['id', 'is_duplicate', 'token_count', 'extracted_quoted_tweet', 'is_retweet', 'contains_keywords', 'created_at']
    df = get_cleaned_data(dtype=dtype, usecols=usecols)
    df_labelled = get_labelled_data(usecols=['tweet_id'], mode=mode)
    df_cleaned_labels_1 = get_cleaned_labelled_data(name=cleaned_labels_name_1, cols=['id'])
    df_cleaned_labels_2 = get_cleaned_labelled_data(name=cleaned_labels_name_2, cols=['id'])
    if include_predictions:
        df_pred = get_predicted_data(include_raw_data=False, dtype=dtype)
        df_pred.index = df.index
        df = pd.concat([df, df_pred], axis=1)
    # compute filters for raw data
    df['d0'] = (df.contains_keywords) & (df.index >= s_date) & (df.index <= e_date)
    df['d1'] = (df.d0) & (df.token_count >= token_count_cutoff)
    df['d2'] = (df.d1) & (~df.is_retweet) & (~df.is_duplicate) & (~df.extracted_quoted_tweet)
    # compute filters for annotation data
    df['a0'] = df.id.isin(df_labelled.tweet_id)
    df['a1'] = df.id.isin(df_cleaned_labels_1.id)
    df['a2'] = df.id.isin(df_cleaned_labels_2.id)
    # compute filters for prediction data
    if include_predictions:
        df['p0'] = df.d1
        df['p1'] = (df.p0) & (df.label_related == 'related')
    return df
